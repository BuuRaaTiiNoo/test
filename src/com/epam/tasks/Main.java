package com.epam.tasks;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class Main {

    //Просто лист
    private List<String> arrayList;
    //Форматироване строки
    private String modString(String s) {
        s = s.replace(" ", "\n")
                .replaceAll("[^A-Za-zА-Яа-я-\n]", "")
                .toLowerCase();
        return s;
    }
    //Компаратор
    class WordComparator implements Comparator<String> {

        @Override
        public int compare(String word1, String word2) {
            return String.valueOf(arrayList.stream().filter(word1::equals).count())
                    .compareTo(String.valueOf(arrayList.stream().filter(word2::equals).count()));
        }
    }

    //в порядке возрастания
    private List<String> getWordByUp(String text) {
        arrayList = new ArrayList<>(Arrays.asList(modString(text).split("\n")));
        return arrayList
                .stream()
                .sorted(new WordComparator().reversed())
                .distinct()
                .limit(20)
                .collect(Collectors.toList());
    }
    //в порядке убывания
    private List<String> getWordByDown(String text) {
        arrayList = new ArrayList<>(Arrays.asList(modString(text).split("\n")));
        return arrayList
                .stream()
                .sorted(new WordComparator())
                .distinct()
                .limit(20)
                .collect(Collectors.toList());
    }

    public static void main(String[] args) {
        String text = "a bnm dfgfhfvb bnm tyu kjhbgv asd isdfop jkl FGasdasdE kjhbjgh ,jhjbgv asd RWE AGD HRT";
        System.out.println("20 самых популярных слов: " + new Main().getWordByUp(text));
        System.out.println("20 самых редкоиспользуемых слов: " + new Main().getWordByDown(text));
    }
}
